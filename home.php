<?php
  $page_title = 'Home Page';
  require_once('includes/load.php');
  if (!$session->isUserLoggedIn(true)) { redirect('index.php', false);}
?>
<?php include_once('layouts/header.php'); ?>
<div class="row">
<div class="col-md-12">
    <?php echo display_msg($msg); ?>
  </div>
  <div class="col-md-12">
    <div class="panel">  
      <div class="jumbotron text-center">
      <h2 style="margin-top:auto;">ACCESOS RAPIDOS</h2>
        <div class="row">
          <div class="col-md-6">
            <div class="panel panel-default">
              <div class="panel-heading">Administrar Salida</div>
              <div class="panel-body">
                <div class="panel-body">
                  <a href="sales.php"><img src="uploads/modulos/admin_sale.png" width="75px" alt=""></a>
                </div>
              </div>
            </div>    
          </div>
          <div class="col-md-6">
            <div class="panel panel-default">
              <div class="panel-heading">Agregar Salida</div>
              <div class="panel-body">
                <div class="panel-body">
                  <a href="add_sale2.php"><img src="uploads/modulos/salida.png" width="75px" alt=""></a>
                </div>
              </div>
            </div>   
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="panel panel-default">
              <div class="panel-heading">Reporte de Salida por Fecha</div>
              <div class="panel-body">
                <div class="panel-body">
                  <a href="sales_report.php"><img src="uploads/modulos/report.png" width="75px" alt=""></a>
                </div>
              </div>
            </div>    
          </div>
          <div class="col-md-6">
            <div class="panel panel-default">
              <div class="panel-heading">Configuración</div>
              <div class="panel-body">
                <div class="panel-body">
                  <a href="edit_account.php"><img src="uploads/modulos/config.png" width="75px" alt=""></a>
                </div>
              </div>
            </div>   
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include_once('layouts/footer.php'); ?>
