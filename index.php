
<?php
  ob_start();
  require_once('includes/load.php');
  if($session->isUserLoggedIn(true)) { redirect('home.php', false);}
?>
<?php include_once('layouts/header.php'); ?>
<style>
  #particles-js{
    display:inline;
  }
    body{
      background-size: cover;
      background-image: linear-gradient(rgba(0, 0, 0, 0.7),rgba(0, 0, 0, 0.7)),url('libs/images/fondo.jpg');
    }
    .page{
      padding: 0px;
    }
</style>
<div class="login-page">
    <div class="text-center">
      <br/>
       <img src="libs/images/logo_ug.png" width="40%" alt="Logo UG">
       <h2>Iniciar sesión </h2>
     </div>
     <?php echo display_msg($msg); ?>
      <form method="post" action="auth.php" class="clearfix">
        <div class="form-group">
              <!--<label for="username" class="control-label">Usuario</label>-->
              <input type="name" class="form-control input-lg" name="username" placeholder="Usuario">
        </div>
        <div class="form-group">
            <!--<label for="password" class="control-label">Contraseña</label>-->
            <input type="password" name= "password" class="form-control  input-lg" placeholder="Contraseña">
        </div>
        <div class="form-group center-items">
                <button type="submit" class="btn btn-primary btn-lg btn-block">ENTRAR</button>
        </div>
    </form>
</div>
<?php include_once('layouts/footer.php'); ?>
