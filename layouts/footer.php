     </div>
    </div>
  
  <script type="text/javascript" src="libs/js/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="cache/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="cache/js/bootstrap-datepicker.min.js"></script>
  
  <script type="text/javascript" src="libs/js/functions.js"></script>

  <!--    particulas   -->   
  <script src="libs/particles/js/particles.js"></script>     
  <script src="libs/particles/js/lib/stats.js"></script>     
  <script src="libs/particles/js/app.js"></script>
  </body>
</html>

<?php if(isset($db)) { $db->db_disconnect(); } ?>
